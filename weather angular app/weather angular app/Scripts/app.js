﻿var weatherapp = angular.module("app", ['ngRoute', 'ngMaterial', 'ngAria', 'ngAnimate', 'ngMessages']);
weatherapp.factory('commonservice', ['$http', '$q', '$rootScope', function ($http, $q, $rootScope) {
    var unit = "metric";
   
   
    var weatherres = [];
    var c,cc=[];
      getweather=function(city, country) {
        
         c = city; //to display same city in result input
         cc = country; //to display same country in result input
         return $http.get('http://api.openweathermap.org/data/2.5/forecast?q=' + city + ',' + country + '&cnt=5&units=' + unit +'&appid=094de3af98ff7527e69a52489ef4fcb4')
            .then(function (data) {
                console.log(data);
                weatherres=data;
            })
            .catch(function (err) {
                console.log('Error getting results');
            })  
     }
      flipb = function () {  
          $("#flipback").removeClass("md-raised").addClass("md-raised md-primary");
          $("#flipfront").removeClass("md-primary").addClass("md-raised");
          $("#flipfront").css("opacity", ".4");
          $("#flipback").css("opacity", "1");
          unit = "metric";
      
      }

      flipf = function () {
          $("#flipfront").removeClass("md-raised").addClass("md-raised md-primary");
          $("#flipback").removeClass("md-primary").addClass("md-raised");
          $("#flipfront").css("opacity", "1");
        
          $("#flipback").css("opacity", ".4");
          unit = "imperial";
         
      }

     var pweather = function () {
         return weatherres;
     }
     var rcity = function () {
         return c;
     }
     var rcountry = function () {
         return cc;
     }
     return {
         getweather: getweather, // return is very important if you define any new function in service, will save time
         rcity:rcity,
         rcountry:rcountry,
         pweather: pweather,
         flipb: flipb,
         flipf:flipf
    };
   

}]);
weatherapp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
    .when('/', {
        templateUrl: '/pages/main.html',
        controller:'mainController'
    })
    .when('/result', {
        templateUrl: '/pages/result.html',
        controller: 'ResultController'
    })
    .otherwise({
        redirectTo:'/'
    })
}])
weatherapp.controller('mainctrl', function ($scope) {
    $scope.heading = "hi this index html";
})

weatherapp.controller('ResultController', ['$scope', 'commonservice', '$rootScope', function ($scope, commonservice, $rootScope) {
    $scope.pageclass = 'page-result';
    //$scope.test = commonservice.putweather();
    
  //  $scope.test = commonservice.getweather.response;
   
    $scope.city = commonservice.rcity();
    $scope.country = commonservice.rcountry();
    function fetchweather(city, country) {
        commonservice.getweather(city, country).then(function (data) {
            $scope.place = commonservice.pweather();     // wait for promise to resolve to execute instruction inside this function
        })
       
        }
    $scope.place = commonservice.pweather();
    $("#flipback").css("opacity", ".4");
    $scope.flipb = function () {
        commonservice.flipb();

    };
    $scope.flipf = function () {
        commonservice.flipf();
    };
    $scope.weathercheck = function (city, country) {
        $scope.place = "";
        fetchweather(city, country);
    };
}])

weatherapp.controller('mainController', ['$scope', 'commonservice', '$location', '$rootScope', function ($scope, commonservice, $location, $rootScope) {
    $scope.pageclass = 'page-home';
    function fetchweather(city, country) {
        commonservice.getweather(city, country).then(function (data) {
          //  alert("gi");
           // $scope.place = data;
            $location.path('/result');
        })
    }
    $("#flipfront").css("opacity", ".4");
    $scope.flipb = function () {
       commonservice.flipb();
       event.preventDefault();
    };
    $scope.flipf = function () {
        commonservice.flipf();
        //  preventDefault();
        return true;
    };
    $scope.weathercheck = function (city, country) {
        
        fetchweather(city, country);
        
    };

    
  
}]
);
//controller('w', ['$scope', 'weatherService', function ($scope, weatherService) {
//    function fetchWeather(zip) {
     
     
//    }

//    fetchWeather('84105');
//}]);